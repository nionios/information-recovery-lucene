#!/bin/sh
# Script to write the paper in an organized manner.
INDIR="paper.tex"
OUTDIR="../../out"
PROJ_ROOT="../../"
RELEASE_NAME="18390126-Nikolopoulos-Lucene.pdf"
# Go through the usal procedure after customization of release name and dir
xelatex --shell-escape $INDIR
echo "\033[38;5;3m*\033[m XeLaTeX finished"
echo "\033[38;5;3m*\033[m Moving .out to other/"
mv paper.out $OUTDIR/other/
echo "\033[38;5;3m*\033[m Moving logs to .logs/"
mv *.log $OUTDIR/logs
echo "\033[38;5;3m*\033[m Making the release name and moving at proj root dir"
cp paper.pdf $PROJ_ROOT/$RELEASE_NAME
echo "\033[38;5;3m*\033[m Moving .pdf to pdf/"
mv paper.pdf $OUTDIR/pdf
echo "Script ended."
if [ "$1" = "--open-pdf" ]
then 
    okular $PROJ_ROOT/$RELEASE_NAME &
    echo "Displaying pdf..."
fi
